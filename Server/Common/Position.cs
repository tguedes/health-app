namespace Server.Common
{
    public class Position
    {
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
    }
}