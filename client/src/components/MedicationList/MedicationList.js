import React, { useState, useEffect } from 'react';

import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {makeStyles} from '@material-ui/core/styles';

import MedicationForm from 'src/components/MedicationForm/MedicationForm';
import {getMedications} from 'src/services/MedicationApiService';

const useStyles = makeStyles({
  tabContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  nameColumn: {
    color: 'blue',
    '&:hover': {
      cursor: 'pointer'
    }
  },
  addNewButton: {
    maxWidth: '225px'
  },
  headerText: {
    textAlign: 'start'
  }
});

const MedicationList = () => {
  const [rows, setRows] = useState([]);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [activeRow, setActiveRow] = useState({});
  const classes = useStyles();

  const loadData = async () => {
    const data = await getMedications();
    console.log(data);
    setRows(data);
  };

  useEffect(() => {
    loadData();
  }, []);

  const handleNewMedicationClick = () => {
    setIsDialogOpen(true);
  };

  const handleDialogClose = () => {
    setIsDialogOpen(false);
    setActiveRow({});
    loadData();
  };

  const handleMedicationClick = (row) => {
    setActiveRow(row);
    setIsDialogOpen(true);
  };

  return (
    <div className={classes.tabContainer}>
      <h1 className={classes.headerText}>Your Appointments</h1>
      <Button onClick={handleNewMedicationClick} variant="contained" color="primary" className={classes.addNewButton}>Add New Medication</Button>
      {isDialogOpen && <MedicationForm {...activeRow} isOpen={isDialogOpen} onClose={handleDialogClose} />}
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Dosage</TableCell>
              <TableCell>Frequency</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows && rows.length > 0 && rows.map((row) => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  <span onClick={() => handleMedicationClick(row)} className={classes.nameColumn}>{row.name}</span>
                </TableCell>
                <TableCell>{row.dosage}</TableCell>
                <TableCell>{row.frequency}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default MedicationList;  