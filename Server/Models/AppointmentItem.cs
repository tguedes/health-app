using System;

namespace Server.Models
{
    public class AppointmentItem
    {
        public long Id { get; set; }
        public string DoctorName { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public string Address { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
    }
}