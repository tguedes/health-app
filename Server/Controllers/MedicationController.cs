using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Server.DTOs;
using Server.Common;
using Server.Models;

namespace Server.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class MedicationController : ControllerBase
    {
        private readonly MedicationContext _context;

        public MedicationController(MedicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MedicationDTO>> GetMedications()
        {
            return Ok(_context.MedicationItems.ToList().Select(a => new MedicationDTO(a)));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MedicationDTO>> GetMedication(long id)
        {
            var item = await _context.MedicationItems.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            return new MedicationDTO(item);
        }

        [HttpPost]
        public async Task<ActionResult<MedicationDTO>> PostMedication(MedicationDTO medication)
        {
            var dbItem = medication.GetDbItem();
            _context.MedicationItems.Add(dbItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction(null, new MedicationDTO(dbItem));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<MedicationDTO>> Updatemedication(long id, MedicationDTO medication)
        {
            if (id != medication.Id)
            {
                return BadRequest();
            }

            var newItem = medication.GetDbItem();
            _context.Entry(newItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.MedicationItems.Any(a => a.Id == id))
                {
                    return NotFound();
                } 
                else {
                    throw;
                }
            }

            return Ok(newItem);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Deletemedication(long id)
        {
            var item = await _context.MedicationItems.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            _context.MedicationItems.Remove(item);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}