using Microsoft.EntityFrameworkCore;

namespace Server.Models
{
    public class MedicationContext : DbContext
    {
        public MedicationContext(DbContextOptions<MedicationContext> options)
            : base(options)
        { }

        public DbSet<MedicationItem> MedicationItems { get; set; }
    }
}