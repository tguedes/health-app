import { GET, POST, PUT } from './ApiService';

const basePath = '/medication';

export const getMedications = async () => {
  const response = await GET(basePath);

  return response;
};

export const postMedication = async (medication) => {
  const response = await POST(basePath, medication);

  return response;
};

export const updateMedication = async (medication) => {
  const path = `${basePath}/${medication.id}`;
  const response = await PUT(path, medication);

  return response;
};