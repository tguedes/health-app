import React, {useState} from 'react';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';

import AppointmentList from 'src/components/AppointmentList/AppointmentList';
import MedicationList from 'src/components/MedicationList/MedicationList';

const TabPanel = ({children, value, index, ...props}) => {
  return (
    <div role="tabpanel" hidden={value !== index} {...props}>
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  )
};

const MainTab = () => {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange}>
          <Tab label="Appointments"/>
          <Tab label="Medications"/>
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <AppointmentList />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <MedicationList />
      </TabPanel>
    </div>
  )
};

export default MainTab;