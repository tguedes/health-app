import React, {useState} from 'react';
import PlacesAutocomplete, {geocodeByAddress, getLatLng} from 'react-places-autocomplete';

const LocationSearchInput = (props) => {
  const [address, setAddress] = useState('');

  const handleChange = address => {
    setAddress(address);
  }

  const handleSelect = address => {
    geocodeByAddress(address)
      .then(results => {
        console.log(results);
        return getLatLng(results[0])
          .then(coordinates => props.onSearchUpdate({coordinates: coordinates, formattedAddress: results[0].formatted_address}))
      })
      .catch(error => console.error('Error', error));
    setAddress('');
  };

  return (
      <PlacesAutocomplete
        value={address}
        onChange={handleChange}
        onSelect={handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            <input
              {...getInputProps({
                placeholder: 'Search Places ...',
                className: 'location-search-input',
              })}
            />
            <div className="autocomplete-dropdown-container">
              {loading && <div>Loading...</div>}
              {suggestions.map(suggestion => {
                const className = suggestion.active
                  ? 'suggestion-item--active'
                  : 'suggestion-item';
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                  : { backgroundColor: '#ffffff', cursor: 'pointer' };
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                  >
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
  );
};

export default LocationSearchInput;