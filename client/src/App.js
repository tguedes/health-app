import React from 'react';
import './App.css';

import MainTab from 'src/components/MainTab/MainTab';

function App() {
  return (
    <div className="App">
      <MainTab />
    </div>
  );
}

export default App;
