# health-app
## Run Locally (Only works on macOS/Linux)
** Requires docker and node/npm **
1. Clone the repo
2. Using the terminal, in root repo directory, run `docker build -t aspnetapp ./Server`
3. Run `docker run -p 8080:80 --name health.server aspnetapp`. The server has now started.
4. Run `cd client` to switch to the client root.
5. Run `npm install`
6. Run `npm start`
7. Navigate to `localhost:3000` in your browser

