import React, { useState, useEffect } from 'react';
import moment from 'moment';

import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {makeStyles} from '@material-ui/core/styles';

import AppointmentForm from 'src/components/AppointmentForm/AppointmentForm';
import {getAppointments} from 'src/services/AppointmentApiService';

const useStyles = makeStyles({
  tabContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  dateColumn: {
    color: 'blue',
    '&:hover': {
      cursor: 'pointer'
    }
  },
  addNewButton: {
    maxWidth: '225px'
  },
  headerText: {
    textAlign: 'start'
  }
});


const AppointmentList = () => {
  const [rows, setRows] = useState([]);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [activeRow, setActiveRow] = useState({});
  const classes = useStyles();

  const loadData = async () => {
    const data = await getAppointments();
    console.log(data);
    setRows(data);
  };

  useEffect(() => {
    loadData();
  }, []);

  const handleAddNewApptClick = () => {
    setIsDialogOpen(true);
  };

  const handleDialogClose = () => {
    setIsDialogOpen(false);
    setActiveRow({});
    loadData();
  };

  const handleAppointmentClick = (row) => {
    const dialogItem = {
      ...row,
      location: row.location ? {lat: row.location.latitude, lng: row.location.longitude} : undefined
    };
    console.log(dialogItem);
    setActiveRow(dialogItem);
    setIsDialogOpen(true);
  };

  return (
    <div className={classes.tabContainer}>
      <h1 className={classes.headerText}>Your Appointments</h1>
      <Button onClick={handleAddNewApptClick} variant="contained" color="primary" className={classes.addNewButton}>Add New Appointment</Button>
      {isDialogOpen && <AppointmentForm {...activeRow} isOpen={isDialogOpen} onClose={handleDialogClose} />}
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Date</TableCell>
              <TableCell>Doctor's Name</TableCell>
              <TableCell align="center">Description</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows && rows.length > 0 && rows.map((row) => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  <span onClick={() => handleAppointmentClick(row)} className={classes.dateColumn}>{moment(row.date).format("MMMM Do YYYY")}</span>
                </TableCell>
                <TableCell>{row.doctorName}</TableCell>
                <TableCell align="center">{row.description}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default AppointmentList;