import React, {useState} from 'react';
import GoogleMapReact from 'google-map-react';

import LocationSearchInput from './LocationSearchInput';
import Marker from './Marker';

const LocationField = (props) => {
  const [zoom, setZoom] = useState(props.zoom);
  const [currentLocation, setCurrentLocation] = useState(props.center);
  const [isAddressSet, setIsAddressSet] = useState(props.isAddressSet);

  const handleSearchUpdate = (address) => {
    props.onUpdateAddressField(address);
    setCurrentLocation(address.coordinates);
    setIsAddressSet(true);
    setZoom(15);
  };

  return (
    <div>
      <div style={{ marginTop: '10px', height: '300px', width: '800px' }}>
        <GoogleMapReact center={currentLocation} zoom={zoom} bootstrapURLKeys={{ key: 'AIzaSyDw1f2koi86FDGcPJv__-H8kehsEYhwUsk'}}>
          {isAddressSet && <Marker lat={currentLocation.lat} lng={currentLocation.lng}/>}
        </GoogleMapReact>
      </div>
      <LocationSearchInput onSearchUpdate={handleSearchUpdate}/>
    </div>
  );
};

LocationField.defaultProps = {
  center: {lat: 27.94, lng: -82.45},
  zoom: 11,
  isAddressSet: false
};

export default LocationField;