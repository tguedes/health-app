namespace Server.Models
{
    public class MedicationItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Dosage { get; set; }
        public string Frequency { get; set; }
    }
}