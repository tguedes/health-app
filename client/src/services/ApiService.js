const baseUrl = () => 'http://localhost:8080/api';

export const GET = async (path, token) => {
  const options = {
    method: 'GET',
  };

  const url = baseUrl() + path;
  let response = await fetch(url, options);

  if (response) {
    response = response.json();
  }

  return response;
};

export const POST = async (path, body, token) => {
  const options = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify(body),
  };

  const url = baseUrl() + path;
  let response = await fetch(url, options);

  if (response) {
    response = response.json();
  } else {
    console.log('API called failed');
  }

  return response;
};

export const PUT = async (path, body, token) => {
  const options = {
    method: 'PUT',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify(body),
  };

  const url = baseUrl() + path;
  let response = await fetch(url, options);

  if (response) {
    response = response.json();
  } else {
    console.log('API called failed');
  }

  return response;
};

export const PATCH = async (path, body, token) => {
  const options = {
    method: 'PATCH',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify(body),
  };

  const url = baseUrl() + path;
  let response = await fetch(url, options);

  if (response) {
    response = response.json();
  }

  return response;
};

export const DELETE = async (path, token) => {
  const options = {
    method: 'DELETE'
  };

  const url = baseUrl() + path;
  let response = await fetch(url, options);

  if (response) {
    response = response.json();
  }

  return response;
};
