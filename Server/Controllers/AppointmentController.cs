using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Server.DTOs;
using Server.Common;
using Server.Models;

namespace Server.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class AppointmentController : ControllerBase
    {
        private readonly AppointmentContext _context;

        public AppointmentController(AppointmentContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AppointmentDTO>> GetAppointments()
        {
            return Ok(_context.AppointmentItems.ToList().Select(a => new AppointmentDTO(a)));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AppointmentDTO>> GetAppointment(long id)
        {
            var item = await _context.AppointmentItems.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            return new AppointmentDTO(item);
        }

        [HttpPost]
        public async Task<ActionResult<AppointmentDTO>> PostAppointment(AppointmentDTO appointment)
        {
            var dbItem = appointment.GetDbItem();
            _context.AppointmentItems.Add(dbItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction(null, new AppointmentDTO(dbItem));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<AppointmentDTO>> UpdateAppointment(long id, AppointmentDTO appointment)
        {
            if (id != appointment.Id)
            {
                return BadRequest();
            }

            var newItem = appointment.GetDbItem();
            _context.Entry(newItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.AppointmentItems.Any(a => a.Id == id))
                {
                    return NotFound();
                } 
                else {
                    throw;
                }
            }

            return Ok(newItem);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAppointment(long id)
        {
            var item = await _context.AppointmentItems.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            _context.AppointmentItems.Remove(item);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}