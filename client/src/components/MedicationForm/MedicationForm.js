import React, {useState} from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles';

import {postMedication, updateMedication} from 'src/services/MedicationApiService';

const useStyles = makeStyles({
  textFieldContainer: {
    display: 'flex',
    flexDirection: 'column'
  }
});

const MedicationForm = ({id, isOpen, onClose, ...props}) => {
  const [formName, setFormName] = useState(props.name);
  const [formDosage, setFormDosage] = useState(props.dosage);
  const [formFrequency, setFormFrequency] = useState(props.frequency);
  const classes = useStyles();

  const handleClose = () => {
    onClose();
  };

  const handleSave = async () => {
    const medication = {
      name: formName,
      dosage: formDosage,
      frequency: formFrequency
    };

    if (id > 0) {
      medication.id = id;
      const result = await updateMedication(medication);
      console.log(result);
    } else {
      const result = await postMedication(medication);
      console.log(result);
    }
    
    handleClose();
  };

  return (
    <Dialog maxWidth='lg' open={isOpen} onClose={handleClose}>
      <DialogTitle>{id > 0 ? `Medication #${id}` : 'New Medication'}</DialogTitle>
      <DialogContent>
        <div className={classes.textFieldContainer}>
          <TextField required label="Name" value={formName} onChange={v => setFormName(v.target.value)}/>
          <TextField required label="Dosage" value={formDosage} onChange={v => setFormDosage(v.target.value)}/>
          <TextField required label="Frequency" value={formFrequency} onChange={v => setFormFrequency(v.target.value)}/>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="secondary">Close</Button>
        <Button onClick={handleSave} color="primary">Save</Button>
      </DialogActions>
    </Dialog>
  );
};

MedicationForm.defaultProps = {
  id: 0,
  name: '',
  dosage: '',
  frequency: ''
};

export default MedicationForm;