using Server.Models;

namespace Server.DTOs
{
    public class MedicationDTO
    {

        public MedicationDTO() { }

        public MedicationDTO(MedicationItem item)
        {
            Id = item.Id;
            Name = item.Name;
            Dosage = item.Dosage;
            Frequency = item.Frequency;
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Dosage { get; set; }
        public string Frequency { get; set; }

        public MedicationItem GetDbItem()
        {
            return new MedicationItem
            {
                Id = Id,
                Name = Name,
                Dosage = Dosage,
                Frequency = Frequency
            };
        }
    }
}