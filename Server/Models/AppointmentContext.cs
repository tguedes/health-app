using Microsoft.EntityFrameworkCore;

namespace Server.Models
{
    public class AppointmentContext : DbContext
    {
        public AppointmentContext(DbContextOptions<AppointmentContext> options)
            : base(options)
        { }

        public DbSet<AppointmentItem> AppointmentItems { get; set; }
    }
}