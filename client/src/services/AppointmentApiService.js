import { GET, POST, PUT } from './ApiService';

const basePath = '/appointment';

export const getAppointments = async () => {
  const response = await GET(basePath);

  return response;
};

export const postAppointment = async (appointment) => {
  const response = await POST(basePath, appointment);

  return response;
};

export const updateAppointment = async (appointment) => {
  const path = `${basePath}/${appointment.id}`;
  const response = await PUT(path, appointment);

  return response;
};