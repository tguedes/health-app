using System;
using Server.Common;
using Server.Models;

namespace Server.DTOs
{
    public class AppointmentDTO
    {
        public AppointmentDTO() { }

        public AppointmentDTO(AppointmentItem item)
        {
            Id = item.Id;
            DoctorName = item.DoctorName;
            Date = item.Date;
            Description = item.Description;
            Address = item.Address;
            Location = item.Longitude != null && item.Latitude != null ? new Position { Longitude = item.Longitude, Latitude = item.Latitude } : null;
        }

        public long Id { get; set; }
        public string DoctorName { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public Position Location { get; set; }

        public AppointmentItem GetDbItem()
        {
            return new AppointmentItem
            {
                Id = Id,
                DoctorName = DoctorName,
                Date = Date,
                Description = Description,
                Address = Address,
                Longitude = Location?.Longitude,
                Latitude = Location?.Latitude
            };
        }
    }
}