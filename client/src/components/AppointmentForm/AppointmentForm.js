import React, {useState} from 'react';
import moment from 'moment';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import MomentUtils from '@date-io/moment';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker, 
  KeyboardDatePicker
} from '@material-ui/pickers';
import {makeStyles} from '@material-ui/core/styles';

import LocationField from './LocationField';
import {postAppointment, updateAppointment} from 'src/services/AppointmentApiService';

const useStyles = makeStyles({
  textFieldContainer: {
    display: 'flex',
    flexDirection: 'column'
  }
});

const AppointmentForm = ({id, isOpen, onClose, ...props}) => {
  const [formDate, setformDate] = useState(props.date);
  const [formDoctorName, setFormDoctorName] = useState(props.doctorName);
  const [formDescription, setFormDescription] = useState(props.description);
  const [formAddress, setFormAddress] = useState(props.address);
  const [coordinates, setCoordinates] = useState(props.location);
  const classes = useStyles();

  const handleDateChange = (date) => {
    setformDate(date);
  };

  const handleClose = () => {
    onClose();
  };

  const handleSave = async () => {
    console.log(id);
    const appt = {
      date: formDate,
      doctorName: formDoctorName,
      description: formDescription,
      location: formAddress ? {longitude: coordinates.lng, latitude: coordinates.lat} : null,
      address: formAddress
    };

    if (id > 0) {
      appt.id = id;
      const result = await updateAppointment(appt);
      console.log(result);
    } else {
      const result = await postAppointment(appt);
      console.log(result);
    }
    
    handleClose();
  };

  const handleUpdateAddressField = (address) => {
    setCoordinates(address.coordinates);
    setFormAddress(address.formattedAddress);
  };

  const getCenter = () => {
    return coordinates != null ? { center: coordinates } : {};
  }

  return (
    <Dialog maxWidth='lg' open={isOpen} onClose={handleClose}>
      <DialogTitle>{props.title ? props.title : 'Appointment'}</DialogTitle>
      <DialogContent>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-picker-inline"
            label="Date"
            value={formDate}
            onChange={handleDateChange}
          />
          <KeyboardTimePicker
            margin="normal"
            id="time-picker"
            label="Time"
            value={formDate}
            onChange={handleDateChange}
          />
        </MuiPickersUtilsProvider>
        <div className={classes.textFieldContainer}>
          <TextField required label="Doctor's Name" value={formDoctorName} onChange={v => setFormDoctorName(v.target.value)}/>
          <TextField label="Description" value={formDescription} onChange={v => setFormDescription(v.target.value)}/>
          <TextField label="Address" disabled value={formAddress}/>
        </div>
        {isOpen ? <LocationField {...getCenter()} onUpdateAddressField={handleUpdateAddressField} isAddressSet={formAddress !== null && formAddress !== ''} /> : null}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="secondary">Close</Button>
        <Button onClick={handleSave} color="primary">Save</Button>
      </DialogActions>
    </Dialog>
  );
};

AppointmentForm.defaultProps = {
  date: moment().format(),
  doctorName: '',
  description: '',
  address: '',
  location: {lat: 27.94, lng: -82.45}
};

export default AppointmentForm;